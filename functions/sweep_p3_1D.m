function [FOM_SWP, SWP_id] = sweep_p3_1D (SWP_id, SWEEP, ASTRA)
%% sweep_p3_1D performs ASTRA parameter scans in 1D
%   it automatically changes the value of a certain parameter, calls successive ASTRA single runs and collects the results.

%   Input:
%       SWP_id = Sweep name. Set to WEP_DDMM_hhmmss if 'new'
%       SWEEP = Parameter sweep information.
%       ASTRA = ASTRA input parameter bundle.

%   Output:
%       FOM_SWP = bundle of figures of merit for each run.
%       SWP_id = Updated SWP_id

%   N. Vallis for PSI for the P^3 project at PSI
    
%% INITIALIZE PROJECT
    w = waitbar(0,'ASTRA sweeep in progress');
    if SWP_id == "new"
        datetime.setDefaultFormats('default','ddMM_HHmmss')
        SWP_id = ['SWP_' char(datetime)];
        mkdir(['astra/' SWP_id])
        mkdir(['astra/' SWP_id '/dat'])
    
        %% SWEEP
        P_field = SWEEP{1};
        P = SWEEP{2};
        FOM_SWP = zeros(length(P),1,19);
        SWP_index_file = [];
        SWP_index_field = [];
%         Phi_1 = ASTRA.Phi_1; Phi_2 = ASTRA.Phi_2;
        for iii = 1:length(P)
            waitbar(iii/length(P));
            ppp = P(iii);
            ASTRA = setfield(ASTRA, P_field, ppp);
%             ASTRA.sol_B_1 = ASTRA.sol_B;
%             ASTRA.sol_B_2 = ASTRA.sol_B;
%             ASTRA.sol_B_3 = ASTRA.sol_B;
%             ASTRA.sol_B_4 = ASTRA.sol_B;
%             ASTRA.Phi_1 = Phi_1 + (P(iii)-0.01)/0.1*360;
%             ASTRA.Phi_2 = Phi_2 + (P(iii)-0.01)/0.1*360;
            [E, FOM, RUN_id] = run_P3 ('new', ['astra/' SWP_id], ASTRA);
            FOM_SWP(iii,1,:) = [FOM.p_prod      FOM.e_prod ...
                                FOM.p_meas      FOM.e_meas ...
                                FOM.p_capt(end) FOM.e_capt(end) ...
                                FOM.p_loss(end) FOM.e_loss(end) ...
                                FOM.E_refc(end) FOM.E_sprp(end) FOM.E_spre(end) ...
                                FOM.x_norm(end) FOM.x_emit(end) FOM.x_rmsw(end) ...
                                FOM.y_norm(end) FOM.y_emit(end) FOM.y_rmsw(end) ...
                                FOM.M_eff_p     FOM.M_eff_p];
            SWP_index_file = [SWP_index_file; RUN_id];
            SWP_index_field = [SWP_index_field; ppp];
        end

        SWP_index = table(SWP_index_file, SWP_index_field);
        SWP_index.Properties.VariableNames = {'RUN id' P_field};
        save(['astra/' SWP_id '/dat/SWP_index'], 'SWP_index');
        
    else
        load(['astra/' SWP_id '/dat/SWP_index'])
        FOM_SWP = zeros(length(unique(SWP_index{:,2})),1,19);
        www = waitbar(0, 'Sweep in progress...');
        for rrr = 1:length(SWP_index{:,2})
            RUN_id = SWP_index{rrr,1};
            RUN_path = ['astra/' SWP_id];
            load([RUN_path '/' RUN_id '/dat/' RUN_id '_ASTRA_parameters'])
            FOM = run_P3 (RUN_id, RUN_path, ASTRA);
            iii = find(unique(SWP_index{:,2}) == SWP_index{rrr,2});
            FOM_SWP(iii,1,:) = [FOM.p_prod      FOM.e_prod ...
                                FOM.p_meas      FOM.e_meas ...
                                FOM.p_capt(end-1) FOM.e_capt(end) ...
                                FOM.p_loss(end) FOM.e_loss(end) ...
                                FOM.E_refc(end) FOM.E_sprp(end) FOM.E_spre(end) ...
                                FOM.x_norm(end) FOM.x_emit(end) FOM.x_rmsw(end) ...
                                FOM.y_norm(end) FOM.y_emit(end) FOM.y_rmsw(end) ...
                                FOM.M_eff_p     FOM.M_eff_p];
            disp([num2str(rrr) ' of ' num2str(length(SWP_index{:,2})) ' runs post-processed.'])
            waitbar(rrr/length(SWP_index{:,2}),www);
        end
    end
    
    save(['astra/' SWP_id '/dat/FOM_SWP'], 'FOM_SWP');
    p_prod = FOM_SWP(:,:,1);  save(['astra/' SWP_id '/dat/p_prod'], 'p_prod');
    e_prod = FOM_SWP(:,:,2);  save(['astra/' SWP_id '/dat/e_prod'], 'e_prod');
    p_capt = FOM_SWP(:,:,5);  save(['astra/' SWP_id '/dat/p_capt'], 'p_capt');
    e_capt = FOM_SWP(:,:,6);  save(['astra/' SWP_id '/dat/e_capt'], 'e_capt');
    p_meas = FOM_SWP(:,:,3);  save(['astra/' SWP_id '/dat/p_meas'], 'p_meas');
    e_meas = FOM_SWP(:,:,4);  save(['astra/' SWP_id '/dat/e_meas'], 'e_meas');
    M_eff_p = FOM_SWP(:,:,18);  save(['astra/' SWP_id '/dat/M_eff_p'], 'M_eff_p');
    M_eff_e = FOM_SWP(:,:,19);  save(['astra/' SWP_id '/dat/M_eff_e'], 'M_eff_e');
end
