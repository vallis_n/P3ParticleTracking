function add_initial_DIST (RUN_id, RUN_path, ASTRA)
%%  add_initial_DIST creates an ASTRA output file in z = 0
%       Not necessary for running ASTRA simulations but helpful for plotting
%       the results
%   Input:
%       RUN_id = project name.
%       RUN_path = parent directory
%       ASTRA = ASTRA input parameter bundle

%   N. Vallis for PSI for the P^3 project at PSI

%%
    DIST_file = ['astra/input_distributions/' ASTRA.dist_file];
    opts = detectImportOptions(DIST_file,'FileType','text');
    DIST = readtable(DIST_file,opts);
    DIST = table2array(DIST);

    % Read Ref. values
    ref_row = DIST(1,:);
    DIST(1,:) = [];

    % Reduction factor
    DIST = DIST(randsample(length(DIST),round(length(DIST)/ASTRA.N_red)),:);

    DIST(:,3) = DIST(:,7)*1e-9 * 3e8 *-1;
    DIST(:,10) = 5 * ones(length(DIST),1);
    DIST(1,:) = ref_row;
    DIST(1,10) = 5;

    DIST = array2table(DIST);

    fid = fopen([RUN_path '/' RUN_id '/' RUN_id '.0000.001.txt'],'w');
    writetable(DIST,[RUN_path '/' RUN_id '/' RUN_id '.0000.001.txt'])
    fclose all
    movefile(fullfile(RUN_path, RUN_id, [RUN_id '.0000.001.txt']), [RUN_path '/' RUN_id '/' RUN_id '.0000.001']);
    
end