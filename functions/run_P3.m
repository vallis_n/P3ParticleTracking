function [FOM, RUN_id, RUN_index] = run_P3 (RUN_id, RUN_path, ASTRA)
%% RUN_P3 writes the input file, runs and reads the results of ASTRA simulations of the P3 layout (HTS + RF Cavities + Solenoids + drift)

%   Input:
%       RUN_id = project name. Set to RUN_DDMM_hhmmss if 'new'
%       RUN_path = parent directory
%       ASTRA = ASTRA input parameter bundle.

%   Output:
%       FOM = Figures of merit in a table
%       RUN_id = Updated RUN_id
%       RUN_index = table including file names and z in mm

%   N. Vallis for PSI for the P^3 project at PSI
    
%% CREATE & RUN ASTRA PROJECT
    switch RUN_id
        case "new"
            % Write input file
            datetime.setDefaultFormats('default','ddMM_HHmmss')
            RUN_id = ['RUN_' char(datetime)];
            wrap_ASTRA_P3(RUN_id, RUN_path, ASTRA); 
            % Run project
            if RUN_path == "astra"
                system(['echo off & cd ' pwd '/astra & astra ' RUN_id '/' RUN_id '.in'], '-echo');
            else
                system(['echo off & cd ' pwd '/astra & astra ' RUN_path(end-14:end) '/' RUN_id '/' RUN_id '.in'], '-echo');
            end
            % add .0000.001 file
            add_initial_DIST(RUN_id, RUN_path, ASTRA)
            RUN_index = get_z_index(RUN_id, RUN_path);
            mkdir([RUN_path '/' RUN_id '/dat'])
            save([RUN_path '/' RUN_id '/dat/' RUN_id '_ASTRA_parameters.mat'], 'ASTRA')
            save([RUN_path '/' RUN_id '/dat/' RUN_id '_RUN_index.mat'], 'RUN_index')
        otherwise
            % Load pre-existing project
            load([RUN_path '/' RUN_id '/dat/' RUN_id '_ASTRA_parameters.mat'])
            load([RUN_path '/' RUN_id '/dat/' RUN_id '_RUN_index.mat'])
    end
    size_dir = size(RUN_index);
    size_dir = size_dir(1);
        
%% EVALUATE
    % Initialize FOMs
    size_dir = size(RUN_index);
    size_dir = size_dir(1);
    
    p_prod = round(137614/ASTRA.N_red); e_prod = round(169140/ASTRA.N_red);
    p_meas = 0; e_meas = 0;
    p_capt = zeros(size_dir,1); e_capt = zeros(size_dir,1);
    p_loss = zeros(size_dir,1); e_loss = zeros(size_dir,1);
    E_refc = zeros(size_dir,1); E_sprp = zeros(size_dir,1); E_spre = zeros(size_dir,1);
    x_norm = zeros(size_dir,1); x_emit = zeros(size_dir,1); x_rmsw = zeros(size_dir,1);
    y_norm = zeros(size_dir,1); y_emit = zeros(size_dir,1); y_rmsw = zeros(size_dir,1);
    B_prof = zeros(size_dir,1); E_prof = zeros(size_dir,1); D_prof = zeros(size_dir,1);
    M_eff_p = 0; M_eff_e = 0;
    
    % Watchdog: all F.O.M.s are set to 0 if RUN is aborted
    if size_dir > 1 
        for fff = 1:size_dir
            DIST_file = strcat(RUN_path, '/', RUN_id, '/', RUN_index.files(fff));
            DIST_file = DIST_file{1};
            fffom = get_DIST_P3(DIST_file, ASTRA);
            p_capt(fff) = fffom(1); e_capt(fff) = fffom(2); 
            p_loss(fff) = fffom(3); e_loss(fff) = fffom(4);
            E_refc(fff) = fffom(5); E_sprp(fff) = fffom(6); E_spre(fff) = fffom(7);

        end
        %[e_meas p_meas] = fcup_P3(ASTRA, DIST_file);
        [x_emit x_norm x_rmsw x_rmsd y_emit y_norm y_rmsw y_rmsd]  = get_astra_emittance(RUN_id, RUN_path);
        [B_prof, E_prof] = get_fields_P3(ASTRA, false);
        [M_eff_p, M_eff_e] = p3_fcups([RUN_path '/' RUN_id '/' RUN_index.files{end}], ASTRA, false);
        p_meas = p_capt(end)*M_eff_p*p_prod*ASTRA.N_red/1e4*200*1e-12;
        e_meas = e_capt(end)*M_eff_e*e_prod*ASTRA.N_red/1e4*200*1e-12;
    end
    
    % Write FOM structure
    FOM = struct('p_prod', p_prod, 'e_prod', e_prod, ...
                 'p_meas', p_meas, 'e_meas', e_meas, ...
                 'p_capt', p_capt, 'e_capt', e_capt, ...
                 'p_loss', p_loss, 'e_loss', e_loss, ...
                 'E_refc', E_refc, 'E_sprp', E_sprp, 'E_spre', E_spre, ...
                 'x_norm', x_norm, 'x_emit', x_emit, 'x_rmsw', x_rmsw, ...
                 'y_norm', y_norm, 'y_emit', y_emit, 'y_rmsw', x_rmsw, ...
                 'B_prof', B_prof, 'E_prof', E_prof, 'D_prof', D_prof, ...
                 'M_eff_p', M_eff_p, 'M_eff_e', M_eff_e);
             
    save([RUN_path '/' RUN_id '/dat/' RUN_id '_FOM.mat'], 'FOM');
        
end