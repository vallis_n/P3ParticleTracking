function [FOM_SWP, SWP_id] = sweep_2D (SWP_id, SWEEP, ASTRA)
%% sweep_p3_2D performs ASTRA parameter scans in 2D
%   it automatically changes the value of certain parameters, calls successive ASTRA single runs and collects the results.

%   Input:
%       SWP_id = Sweep name. Set to WEP_DDMM_hhmmss if 'new'
%       SWEEP = Parameter sweep information.
%       ASTRA = ASTRA input parameter bundle.

%   Output:
%       FOM_SWP = bundle of figures of merit for each run.
%       SWP_id = Updated SWP_id

%   N. Vallis for PSI for the P^3 project at PSI

    %% INITIALIZE PROJECT
    if SWP_id == "new"
        datetime.setDefaultFormats('default','ddMM_HHmmss')
        SWP_id = ['SWP_' char(datetime)];
        mkdir(['astra/' SWP_id])
        mkdir(['astra/' SWP_id '/dat'])
            
        %% SWEEP
        P1_field = SWEEP{1,1};
        P2_field = SWEEP{1,2};
        P1 = SWEEP{2,1};
        P2 = SWEEP{2,2};
        FOM_SWP = zeros(length(P1),length(P2),19);
        SWP_index_file = [];
        SWP_index_field1 = [];
        SWP_index_field2 = [];
        for iii = 1:length(P1)
            for jjj = 1:length(P2)
                ppp1 = P1(iii);
                ppp2 = P2(jjj);
                ASTRA = setfield(ASTRA, P1_field, ppp1);
                ASTRA = setfield(ASTRA, P2_field, ppp2);
%                 % attention!
%                 ASTRA = setfield(ASTRA, RF_Phi, ppp2);
                [E, FOM, RUN_id] = run_P3 ('new', ['astra/' SWP_id], ASTRA);
                FOM_SWP(iii,jjj,:) = [FOM.p_prod      FOM.e_prod ...
                                FOM.p_meas      FOM.e_meas ...
                                FOM.p_capt(end) FOM.e_capt(end) ...
                                FOM.p_loss(end) FOM.e_loss(end) ...
                                FOM.E_refc(end) FOM.E_sprp(end) FOM.E_spre(end) ...
                                FOM.x_norm(end) FOM.x_emit(end) FOM.x_rmsw(end) ...
                                FOM.y_norm(end) FOM.y_emit(end) FOM.y_rmsw(end) ...
                                FOM.M_eff_p     FOM.M_eff_p];
                SWP_index_file = [SWP_index_file; RUN_id];
                SWP_index_field1 = [SWP_index_field1; ppp1];
                SWP_index_field2 = [SWP_index_field2; ppp2];
            end
        end  
    SWP_index = table(SWP_index_file, SWP_index_field1, SWP_index_field2);
    SWP_index.Properties.VariableNames = {'RUN id' P1_field P2_field};
    save(['astra/' SWP_id '/dat/SWP_index'], 'SWP_index');
    
    else 
        load(['astra/' SWP_id '/dat/SWP_index'])
        FOM_SWP = zeros(length(unique(SWP_index{:,2})),length(unique(SWP_index{:,3})),19);
        www = waitbar(0, 'Sweep in progress...');
        for rrr = 1:length(SWP_index{:,2})
            RUN_id = SWP_index{rrr,1};
            RUN_path = ['astra/' SWP_id];
            load([RUN_path '/' RUN_id '/dat/' RUN_id '_ASTRA_parameters'])
            FOM = run_P3 (RUN_id, RUN_path, ASTRA);
            iii = find(unique(SWP_index{:,2}) == SWP_index{rrr,2});
            jjj = find(unique(SWP_index{:,3}) == SWP_index{rrr,3});
            FOM_SWP(iii,jjj,:) = [FOM.p_prod      FOM.e_prod ...
                                FOM.p_meas      FOM.e_meas ...
                                FOM.p_capt(end-1) FOM.e_capt(end) ...
                                FOM.p_loss(end) FOM.e_loss(end) ...
                                FOM.E_refc(end) FOM.E_sprp(end) FOM.E_spre(end) ...
                                FOM.x_norm(end) FOM.x_emit(end) FOM.x_rmsw(end) ...
                                FOM.y_norm(end) FOM.y_emit(end) FOM.y_rmsw(end) ...
                                FOM.M_eff_p     FOM.M_eff_e];
            disp([num2str(rrr) ' of ' num2str(length(SWP_index{:,2})) ' runs post-processed.'])
            waitbar(rrr/length(SWP_index{:,2}),www);
        end
    end
    
    FOM_SWP(FOM_SWP == 0) = NaN;
    save(['astra/' SWP_id '/dat/FOM_SWP'], 'FOM_SWP');
    p_prod = FOM_SWP(:,:,1);  save(['astra/' SWP_id '/dat/p_prod'], 'p_prod');
    e_prod = FOM_SWP(:,:,2);  save(['astra/' SWP_id '/dat/e_prod'], 'e_prod');
    p_capt = FOM_SWP(:,:,5);  save(['astra/' SWP_id '/dat/p_capt'], 'p_capt');
    e_capt = FOM_SWP(:,:,6);  save(['astra/' SWP_id '/dat/e_capt'], 'e_capt');
    p_meas = FOM_SWP(:,:,3);  save(['astra/' SWP_id '/dat/p_meas'], 'p_meas');
    e_meas = FOM_SWP(:,:,4);  save(['astra/' SWP_id '/dat/e_meas'], 'e_meas');
    M_eff_p = FOM_SWP(:,:,18);  save(['astra/' SWP_id '/dat/M_eff_p'], 'M_eff_p');
    M_eff_e = FOM_SWP(:,:,19);  save(['astra/' SWP_id '/dat/M_eff_e'], 'M_eff_e');
end