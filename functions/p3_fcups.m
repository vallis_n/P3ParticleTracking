function [M_eff_p, M_eff_e] = p3_fcups(filename, ASTRA, plot_flag)
%%  p3_fcups calculates the amount of e+ and e- intercepted by the Faraday Cups

%   Input:
%       filename = [RUN_path '/' RUN_id '/' RUN_index.files{end}].
%       ASTRA = ASTRA input parameter bundle.
%       plot_flag = activate to plot Faraday cup layout and particle distributions at Fcup surface.

%   Output:
%       M_eff_p = proportion of e+ intercepted by Fcup.
%       M_eff_e = proportion of e- intercepted by Fcup.

%   N. Vallis for PSI for the P^3 project at PSI

%% MODIFY fcup_p BELOW TO OVERWRITE 'ASTRA' BUNDLE WITH DIFFERENT FCUP GEOMETRY

fcup_p = ASTRA.fcup_p;
%fcup_p = [350+80/2*cosd(75)*[-1 1]; -(100+sind(75)*80) -100]/1e3;
% fcup_p = [400+260/2*cosd(85)*[-1 1]; -(165+260/2*sind(85)*[1 -1])]/1e3;
y_fcup_p = ASTRA.y_fcup_p;
h_fcup_p = ASTRA.h_fcup_p;

fcup_e = [fcup_p(1,:); -1*fcup_p(2,:)]; % symmetric Fcup for electrons

%% 

% Get distribution
opts = detectImportOptions(filename, 'FileType', 'text');
DIST = readtable(filename,opts);
DIST = table2array(DIST);
REF = DIST(1,:);
DIST = DIST(DIST(:,10) == 5,:);
DIST = DIST(DIST(:,10) == 5 & DIST(:,3)+REF(3) > ASTRA.D_pos+ASTRA.D_drift,:);
pDIST = DIST(DIST(:,9) == 2,:);
eDIST = DIST(DIST(:,9) == 1,:);

% Post-Processing: Positrons
DIST = pDIST;
z = DIST(:,3)+REF(3)-3.2;
x = DIST(:,1);
y = DIST(:,2);
x_prime = DIST(:,4)./(DIST(:,6)+REF(6));
y_prime = DIST(:,5)./(DIST(:,6)+REF(6));
x_0 = x - z.*x_prime;
y_0 = y - z.*y_prime;
f_prime = (fcup_p(2,2)-fcup_p(2,1))/(fcup_p(1,2)-fcup_p(1,1));
f_0 = fcup_p(2,2) - fcup_p(1,2)*f_prime;
z_dist_fcup = (x_0-f_0)./(f_prime-x_prime);
% z_dist_fcup = ASTRA.fcup_p(1,2)*ones(length(x),1);
x_dist_fcup  = x - (z - z_dist_fcup) .* x_prime;
y_dist_fcup = y - (z - z_dist_fcup) .* y_prime;
charge = 2*(DIST(:,9))-3;
Np = length(find(charge==1));

MeasIndex_p = find(x_dist_fcup>fcup_p(2,1) & x_dist_fcup<fcup_p(2,2) & y_dist_fcup>y_fcup_p-h_fcup_p/2 & y_dist_fcup<y_fcup_p+h_fcup_p/2);
Interf_p = find(x_dist_fcup<fcup_e(2,1) & x_dist_fcup>fcup_e(2,2) & y_dist_fcup>y_fcup_p-h_fcup_p/2 & y_dist_fcup<y_fcup_p+h_fcup_p/2);

% Post-Processing: Electrons
DIST = eDIST;
z = DIST(:,3)+REF(3)-3.2;
x = DIST(:,1);
y = DIST(:,2);
x_prime = DIST(:,4)./(DIST(:,6)+REF(6));
y_prime = DIST(:,5)./(DIST(:,6)+REF(6));
x_0 = x - z.*x_prime;
y_0 = y - z.*y_prime;
f_prime = (fcup_p(2,2)-fcup_p(2,1))/(fcup_p(1,2)-fcup_p(1,1));
f_0 = fcup_p(2,2) - fcup_p(1,2)*f_prime;
z_dist_fcup = (x_0-f_0)./(f_prime-x_prime);
% z_dist_fcup = ASTRA.fcup_p(1,2)*ones(length(x),1);
x_dist_fcup  = x - (z - z_dist_fcup) .* x_prime;
y_dist_fcup = y - (z - z_dist_fcup) .* y_prime;
charge = 2*(DIST(:,9))-3;
Ne = length(find(charge==-1));


MeasIndex_e = find(x_dist_fcup<fcup_e(2,1) & x_dist_fcup>fcup_e(2,2) & y_dist_fcup>y_fcup_p-h_fcup_p/2 & y_dist_fcup<y_fcup_p+h_fcup_p/2);
Interf_e = find(x_dist_fcup>fcup_p(2,1) & x_dist_fcup<fcup_p(2,2) & y_dist_fcup>y_fcup_p-h_fcup_p/2 & y_dist_fcup<y_fcup_p+h_fcup_p/2);

M_eff_p = (length(MeasIndex_p)-length(Interf_e))/Np;
M_eff_e = (length(MeasIndex_e)-length(Interf_p))/Ne;

disp([length(Interf_e) length(Interf_p)])

%% plot
 if plot_flag
    figure()
    scatter(z_dist_fcup*1e3, x_dist_fcup*1e3, 15,sqrt((DIST(:,6)+REF(6)).^2 + DIST(:,4).^2)/1e6, 'filled')
    %scatter(z_dist_fcup*1e3, x_dist_fcup*1e3, 15,(DIST(:,6)+REF(6))/1e6, 'filled')
    colorbar
    caxis([0 100])
    hold on
    scatter(fcup_p(1,1)*1e3,fcup_p(2,1)*1e3,50,'r', 'filled')
    scatter(fcup_p(1,2)*1e3,fcup_p(2,2)*1e3,50,'r', 'filled')
    hold off
    
    figure()
    scatter(x_dist_fcup*1e3, y_dist_fcup*1e3, 15,sqrt((DIST(:,6)+REF(6)).^2 + DIST(:,4).^2)/1e6, 'filled')
    hold on
        rectangle('Position',[fcup_p(2,1)*1e3, (y_fcup_p-h_fcup_p/2)*1e3, (fcup_p(2,2)-fcup_p(2,1))*1e3, h_fcup_p*1e3])
        colorbar
        caxis([0 100])
 end


end

