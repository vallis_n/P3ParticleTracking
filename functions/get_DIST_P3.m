function [fffom, DIST_e, DIST_p, REF] = get_DIST_P3(DIST_file, ASTRA)
%%  get_DIST_P3 gets ASTRA particle distribution at a given z 
%   and calculates the correspondent figures of merit

%   Input:
%       DIST_file = ASTRA output filename (.001)
%       ASTRA = ASTRA input parameter bundle

%   Output:
%       DIST_e = e- distribution
%       DIST_p = e+ distribution
%       REF = coordinates of the reference particle
%       fffom = row of the FOM table

%   N. Vallis for PSI for the P^3 project at PSI

    %% Physical constants & properties
    c = 299792458;
    f = ASTRA.fff*1e9;
    lambda = c/f;

    %% READ FILE
    opts = detectImportOptions(DIST_file,'FileType','text');
    DIST = readtable(DIST_file,opts);
    DIST = table2array(DIST);
    % Set custom units [mm, MeV, ns]
    DIST(:,1) = DIST(:,1) * 1e3;
    DIST(:,2) = DIST(:,2) * 1e3;
    DIST(:,3) = DIST(:,3) * 1e3;
    DIST(:,4) = DIST(:,4) / 1e6;
    DIST(:,5) = DIST(:,5) / 1e6;
    DIST(:,6) = DIST(:,6) / 1e6;
    % Reference particle
    REF = DIST(1,:);
    % Energy function
%     E_0 = sqrt(0.511^2 + REF(4)^2+REF(5)^2+REF(6)^2);
%     E = @(DIST) sqrt(0.511^2 + DIST(:,4).^2+DIST(:,5).^2+(DIST(:,6)+REF(6)).^2);
%     DIST(:,6) = E(DIST);
    % Separate e-
    DIST_e = DIST(DIST(:,9)==1,:); 
    N_e = length(DIST_e);
    DIST_e = DIST_e(DIST_e(:,10)==5,:);
    % Separate e+
    DIST_p = DIST(DIST(:,9)==2,:);
    DIST_p(1,:) = [];
    N_p = length(DIST_p);
    DIST_p = DIST_p(DIST_p(:,10)==5,:);
        
    %% FOMs
    fffom = zeros(1,7);
    % Captured positron efficiency
    fffom(1) = length(DIST_p)/N_p;
    % Captured electron efficiency
    fffom(2) = length(DIST_e)/N_e;
    % Lost Positrons
    fffom(3) = 1 - fffom(1);
    % Lost electrons
    fffom(4) = 1 - fffom(2);
    % Reference energy
    fffom(5) = REF(6);
    % Energy spread positrons
    fffom(6) = std(DIST_p(:,6));
    % Energy spread electrons
    fffom(7) = std(DIST_e(:,6));
    
end