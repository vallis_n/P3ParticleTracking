function [x_emit x_norm x_rmsw x_rmsd y_emit y_norm y_rmsw y_rmsd z_xemit z_yemit] = get_astra_emittance(RUN_id, RUN_path)
%%  get_astra_emittance reads the emittance calculated by ASTRA 

%   Input:
%       RUN_id = project name.
%       RUN_path = project parent directory.

%   Output:
%       x_norm, x_rmsw, x_rmsd = normalized emittance, rms beam size and
%       rms beam divergence in x.
%       y_norm, y_rmsw, y_rmsd = normalized emittance, rms beam size and
%       rms beam divergence in y.
%       z_xemit, z_yemit = z values where the above figures are calculated
%       in x and y.

%   N. Vallis for PSI for the P^3 project at PSI

    Xemit = readmatrix([RUN_path '/' RUN_id '/' RUN_id '.Xemit.001'],'FileType','text','ExpectedNumVariables',7,'OutputType','double');
    [u, ia, ic] = unique(Xemit(:,1));
    
    Xemit = Xemit(ia,:);    
    x_norm = Xemit(:,6);
    x_rmsw = Xemit(:,4);
    x_rmsd = Xemit(:,5);
    x_px_avg = Xemit(:,7);
    z_xemit = Xemit(:,1);
    
    x_emit = sqrt(x_rmsw.^2 .* x_rmsd.^2 - x_px_avg);
    
    Yemit = readmatrix([RUN_path '/' RUN_id '/' RUN_id '.Yemit.001'],'FileType','text','ExpectedNumVariables',7,'OutputType','double');
    [u, ia, ic] = unique(Yemit(:,1));
    
    Yemit = Yemit(ia,:);    
    y_norm = Yemit(:,6);
    y_rmsw = Yemit(:,4);
    y_rmsd = Yemit(:,5);
    y_py_avg = Yemit(:,7);
    z_yemit = Yemit(:,1);
    
    y_emit = sqrt(y_rmsw.^2 .* y_rmsd.^2 - y_py_avg);
    
end