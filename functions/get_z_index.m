function RUN_index = get_z_index(RUN_id, RUN_path)
%%  get_z_index Indexes every ASTRA distribution file (.001) in a directory
%       directory and their corresponding position in the z axis
%   Input:
%       RUN_id = project name. 
%       RUN_path = project parent directory
%   Output:
%       RUN_index = table including file names and z in mm

%   N. Vallis for PSI for the P^3 project at PSI

    files = dir([RUN_path '/' RUN_id]); % index distribution files
    files = {files.name}.';
    RUN_index = table(files); % create first column of index list
    RUN_index(1:2,:) = []; RUN_index(end-8:end,:) = []; 
    
    z = []; % initialize variable for second column
    for iii = 1:length(RUN_index.files)
        zzz = RUN_index.files(iii);
        zzz = strsplit(zzz{1}, '.');
        zzz = str2num(zzz{2}) * 10;
        z = [z; zzz];
    end
    
    % watchdog: RUN_index is empty if ASTRA simulation fails
    if isempty(z)
        RUN_index = [];
    else
        RUN_index.z = z;
    end
    
end