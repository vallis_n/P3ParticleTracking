function fig_long = plot_long_p3(dist_file, RUN_index, ASTRA, FOM, disp_flag)
%% plot_long_p3 plots the longitudinal field profile of a given ASTRA simulation at a given z.

%   Input:
%       dist_file = ASTRA output file to plot.
%       ASTRA = ASTRA input parameter bundle.
%       FOM = Figures of merit output parameter bundle.
%       disp_flag = Deactivate to hide figures (e.g. to make a .gif movie)

%   N. Vallis for PSI for the P^3 project at PSI

[fffom, DIST_e, DIST_p, REF] = get_DIST_P3(dist_file, ASTRA);

color_e = [0.121568627450980,0.466666666666667,0.705882352941177];
color_p = [215, 68, 38]/255;

    %% PLOT
    if disp_flag
        fig_long = figure('Name', 'Output Distribution', 'Units', 'Normalized', 'OuterPosition', [0 0 0.45 0.40]);
    else
        fig_long = figure('Name', 'Output Distribution', 'Units', 'Normalized', 'OuterPosition', [0 0 0.45 0.40], 'Visible', 'off');
    end
    set(gcf, 'color', 'w')
    
    xmin = -200; xmax = 100;
    ymin = -100; ymax = 100;

    %% E vs. z
    subplot(4,6,[2:4 , 8:10 , 14:16])
    scatter(DIST_e(:,3),DIST_e(:,6), 0.1, "MarkerEdgeColor", color_p, 'MarkerEdgeAlpha',.5)
    hold on
    scatter(DIST_p(:,3),DIST_p(:,6), 0.1, "MarkerEdgeColor", color_e, 'MarkerEdgeAlpha',.5)
    xline(0,'-',['Ref. z'],'LabelVerticalAlignment','bottom')
    yline(0,'-',['Ref. Energy'],'LabelHorizontalAlignment','left')
    hold off
    % Scale
        xlim([xmin xmax])
        ylim([ymin ymax])
        % Presentation
        ax3 = gca;
        set(gca,'xticklabel',[],'yticklabel',[])
        legend('e-','e+')

    %% Z hist
    subplot(4,6,20:22)
    hxe = histogram(DIST_e(:,3),'Normalization','probability') ;
    hxe.FaceColor = color_p;
    hxe.EdgeColor = 'none';
    hxe.BinWidth = 2;
    hold on
    hxp = histogram(DIST_p(:,3),'Normalization','probability');
    hxp.FaceColor = color_e;
    hxp.EdgeColor = 'none';
    hxp.BinWidth = 2;
    hold off
        % Scale
        xlim([xmin xmax])
        ylim('auto')
        % Presentation
        xlabel('z [mm]')
        ylabel('%')
        ax1 = gca;
        set(ax1,'YTickLabels',ax1.YTick*100)

    %% E hist
    subplot(4,6,[1,7,13])
    hye = histogram(DIST_e(:,6),'Normalization','probability','Orientation','horizontal');
    hye.FaceColor = color_p;
    hye.EdgeColor = 'none';
    hye.BinWidth = 2;
    hold on
    hyp = histogram(DIST_p(:,6),'Normalization','probability','Orientation','horizontal');
    hyp.FaceColor = color_e;
    hyp.EdgeColor = 'none';
    hyp.BinWidth = 2;
    hold off
        % Scale
        ylim([ymin ymax])
        xlim('auto')
        % Presentation
        ylabel('E [MeV]')
        xlabel('%')
        ax2 = gca;
        set(ax2,'XTickLabels',ax2.XTick*100)

    %% Energy over z
        % Load data
        E_refc = FOM.E_refc;
    subplot(4,6,[5,6,11,12])
    plot(RUN_index.z/1e3,E_refc,'Linewidth',1.5,'Color',[64 64 64]/255)
    hold on
    scatter(REF(3)/1e3,REF(6),50,'MarkerEdgeColor',[255 51 51]/255,'MarkerFaceColor',[255 51 51]/255)
    hold off
        % Presentation
        ax4 = gca;
        ax4.YAxisLocation = 'right';
        ylabel('Ref. Energy [MeV]')
        legend(['E_{ref} = ' num2str(REF(6)) ' MeV'], ['z_{ref} = ' num2str(REF(3)) ' mm'], 'Location', 'northwest')
        xlim([0 3])

    %% e+e- Survival over z
    subplot(4,6,[17,18,23,24])
    plot(RUN_index.z/1e3,FOM.e_capt*1e2,'-','Linewidth',1.5,'Color', color_e)
    hold on
    plot(RUN_index.z/1e3,FOM.p_capt*1e2,'-','Linewidth',1.5,'Color', color_p)
    scatter(REF(3)/1e3,fffom(2)*100,20,'MarkerEdgeColor',color_e,'MarkerFaceColor',color_e)
    scatter(REF(3)/1e3,fffom(1)*100,20,'MarkerEdgeColor',color_p,'MarkerFaceColor',color_p)
    hold off
        % Presentation
        ax5 = gca;
        ax5.YAxisLocation = 'right';
        ylabel('Captured particles [%]')
        xlabel('z [m]')
        legend(['Captured e- (' num2str(fffom(2)*100) ' %)'],['Captured e+ (' num2str(fffom(1)*100) ' %)'])
        ylim([0 100])
        xlim([0 2.7])
        
    %% synchronize axes
    linkaxes([ax1 ax3],'x')
    linkaxes([ax2 ax3],'y')
    
end