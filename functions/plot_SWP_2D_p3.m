function plot_SWP_2D_p3(SWP_id, SWEEP, fom_name)
%% sweep_p3_2D plots a specific FOM resulting from an ASTRA parameter scan in 2D

%   Input:
%       SWP_id = Sweep name.
%       SWEEP = Parameter sweep information.
%       fom_name = specific FOM name.

%   N. Vallis for PSI for the P^3 project at PSI
    
    %% Load Data
    fom = load(['astra/' SWP_id, '/dat/' fom_name '.mat']);
    fom = struct2array(fom);
    
    %% Dictionaries 
    fieldSet = {'p_prod', 'e_prod', 'p_capt', 'e_capt', ...
               'p_meas', 'e_meas', 'M_eff_p', 'M_eff_e'};
           
    fieldLegend =  {'Positrons generated', ...
                    'Electrons generated', ...
                    'e+ global capture efficiency', ...
                    'e- global capture efficiency', ...
                    'Measured positron charge', ...
                    'Measured electron charge', ...
                    'Positron charge measurement efficiency', ...
                    'Electron charge measurement efficiency'};
    field_dic = containers.Map(fieldSet, fieldLegend);
               
    % tbd: param_dic = {'Phi_1', etc};

    %% Plot 
    surf(SWEEP{2,2}, SWEEP{2,1}, fom);
        % Presentation
        xlabel(SWEEP{1,2})
        ylabel(SWEEP{1,1})
        title(field_dic(fom_name))
    
end