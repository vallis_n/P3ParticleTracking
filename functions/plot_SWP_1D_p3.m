function plot_SWP_1D_p3(SWP_id, fom_name, x_label)
%% sweep_p3_1D plots a specific FOM resulting from an ASTRA parameter scan in 1D

%   Input:
%       SWP_id = Sweep name.
%       SWEEP = Parameter sweep information.
%       fom_name = specific FOM name.
%       x_label = custom x label for the plots. Normally the swept parameter.

%   N. Vallis for PSI for the P^3 project at PSI
    
    %% Load Data
    fom = load(['astra/' SWP_id, '/dat/' fom_name '.mat']);
    load(['astra/' SWP_id, '/dat/SWP_index.mat']);
    fom = struct2array(fom);
    
    %% Dictionaries 
    fieldSet = {'p_prod', 'e_prod', 'p_capt', 'e_capt', ...
               'p_meas', 'e_meas', 'M_eff_p', 'M_eff_e'};
           
    fieldLegend =  {'Positrons generated', ...
                    'Electrons generated', ...
                    'e+ global capture efficiency', ...
                    'e- global capture efficiency', ...
                    'Measured positron charge', ...
                    'Measured electron charge', ...
                    'Positron charge measurement efficiency', ...
                    'Electron charge measurement efficiency'};
                
    field_dic = containers.Map(fieldSet, fieldLegend);
               
    % tbd: param_dic = {'Phi_1', etc};

    %% Plot 
    plot(SWP_index{:,2}, fom, '-o');
    %plot(fom, '-o');
        % Presentation
        xlabel(x_label)
        title(field_dic(fom_name))
    
end