function [B_prof, E_prof] = get_fields_P3(ASTRA, plot_flag)
%%  get_fields_P3 reconstructs the magnetic, electric and dipole fieldmaps used by ASTRA 

%   Input:
%       ASTRA = ASTRA input parameter bundle
%       plot_flag = activate to plot the fields

%   Output:
%       B_prof = solenoid field profile on axis.
%       E_prof = RF accelerating field profile on axis.

%   N. Vallis for PSI for the P^3 project at PSI

%%
    global c_map
    
    % field domain in z
    z = [ASTRA.ZSTART-0.5:0.001:ASTRA.ZSTOP]';
    
    %  RF fields
    RF_field_single = importdata(join(["astra/", ASTRA.RF_fieldmap], ''));
    E_prof = zeros(length(z),1);
        sum_field = interp1(RF_field_single(:,1)+ASTRA.C_pos_1, RF_field_single(:,2), z);
        sum_field(isnan(sum_field)) = 0;
    E_prof = E_prof + sum_field;
        sum_field = interp1(RF_field_single(:,1)+ASTRA.C_pos_2, RF_field_single(:,2), z);
        sum_field(isnan(sum_field)) = 0;
    E_prof = E_prof + sum_field;
    
    % Solenoid fields
    AMD_field = importdata(join(["astra/", ASTRA.AMD_fieldmap], ''));
        AMD_field = interp1(AMD_field(:,1), AMD_field(:,2), z);
        AMD_field(isnan(AMD_field)) = 0;
    sol_field_single = importdata(join(["astra/", ASTRA.sol_fieldmap], ''));
    B_prof = AMD_field;
    dddsol = ASTRA.sol_dist;
    s_pos = [ASTRA.C_pos_1-dddsol, ASTRA.C_pos_1+dddsol, ASTRA.C_pos_2-dddsol, ASTRA.C_pos_2+dddsol];
    for i = 1:4
            sum_field = interp1(sol_field_single(:,1)+s_pos(i), sol_field_single(:,2), z);
            sum_field(isnan(sum_field)) = 0;
        B_prof = B_prof + sum_field;
    end
    
    
    if plot_flag
        f = figure("Position",[0 0 1000 200]);
        yyaxis left
        plot(z, AMD_field, 'Color', c_map(1,:), 'LineWidth', 2)
        ylabel('AMD Field on axis [T]')
        yyaxis right
        plot(z, B_prof, 'Color', c_map(2,:), 'LineWidth', 2)
        xlim([-0.5 ASTRA.ZSTOP+0.5])
        ylabel('Sol. Field on axis [T]')
        xlabel('z [m]')
        ax2 = gca;
        ax2.YAxis(1).Color = c_map(1,:);
        ax2.YAxis(2).Color = c_map(2,:);
        
        f = figure("Position",[0 0 1000 200]);
        plot(z, E_prof, 'Color', c_map(3,:), 'LineWidth', 2)
        xlim([-0.5 ASTRA.ZSTOP+0.5])
        ylabel('RF Field on axis [V/m]')
        
        f = figure("Position",[0 0 1000 200]);
        plot(z, D_prof, 'Color', c_map(4,:), 'LineWidth', 2)
        xlim([-0.5 ASTRA.ZSTOP+0.5])
        ylabel('Dipole Field on axis [T]')
        xlabel('z [m]')


    end
end

