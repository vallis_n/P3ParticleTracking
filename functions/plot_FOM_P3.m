function plot_FOM_P3(FOM, ASTRA, RUN_index, RUN_id, RUN_path)
%% plot_FOM_P3 plots some FOMs of a given ASTRA simulation.

%   Input:
%       ASTRA = ASTRA input parameter bundle.
%       FOM = Figures of merit output parameter bundle.
%       RUN_id = project name.
%       RUN_path = parent directory

%   N. Vallis for PSI for the P^3 project at PSI

    global c_map
    
    z = RUN_index.z /1000; % z in meters
    
    figure('Renderer', 'painters', 'Position', [0 0 900 900])
    
        % Energy and Energy spread
        subplot(4,1,1)
        hold on
        plot(z, FOM.E_refc, 'Color', c_map(1,:))
        hold off
            xlim([0 2.8])
            ylim([0 55])
            ylabel('E [MeV]')
            grid on

        % Capture efficiency and loss
        subplot(4,1,2)
        yyaxis left
        plot(z, FOM.p_capt*200*13.8, 'Color', c_map(3,:))
            set(gca, 'ycolor', 'k')
            ylabel('Captured charge [pC]')
            ylim([0.35 1]*200*13.8)
            grid on
        yyaxis right
        b = bar(z(1:end-6), diff(FOM.p_loss(6:end))*200*13.8);
            b.FaceColor = c_map(4,:);
            b.EdgeColor = 'none';
            b.FaceAlpha = 0.7;
            set(gca, 'ycolor', c_map(2,:))
            ylabel('local e+ loss [pC]')
            legend('e+ Capture', 'Local loss', 'Location', 'north', 'Orientation', 'horizontal', 'Box', 'off')
            xlim([0 2.8])
            ylim([0 0.05]*200*13.8)
        % Emittance
        [x_emit x_norm x_rmsw x_rmsd y_emit y_norm y_rmsw y_rmsd z_xemit z_yemit]  = get_astra_emittance(RUN_id, RUN_path);
        ax3 = subplot(4,1,3);
        
        %yyaxis left
        plot(z_yemit, FOM.x_norm, 'Color', c_map(7,:))
            set(gca, 'ycolor', 'k')
            ylabel('\epsilon_{xnorm} [\pi mm mrad]')
            ylim([0 35000])
            grid on
        yyaxis right
        plot(z_yemit, FOM.x_rmsw, 'Color', c_map(8,:))
            set(gca, 'ycolor', c_map(2,:))
            ylabel('\sigma_{x} [mm]')
            legend('\epsilon_{norm}', '\sigma_{x}', 'Location', 'north', 'Orientation', 'horizontal', 'Box', 'off')
        ylim([0 10])
        xlim([0 2.8])

        % RF and Magnet fields
        ax4 = subplot(4,1,4);
        hold on
        [B_prof, E_prof] = get_fields_P3(ASTRA, 0);
        plot([ASTRA.ZSTART-0.5:0.001:ASTRA.ZSTOP]', B_prof, 'Color', c_map(5,:))
            set(gca, 'ycolor', 'k')
            ylabel('Sol. field [T]')
            xlabel('z [m]')
            grid on
            ylim([0 13])
        yyaxis right
        plot([ASTRA.ZSTART-0.5:0.001:ASTRA.ZSTOP]', E_prof/1e6, 'Color', [0.9290 0.6940 0.1250]);
            set(gca, 'ycolor', c_map(2,:))
            ylabel('E. field [MV/m]')
            ylim([-1 1]*70)
        hold off
        xlim([0 2.8])
        legend('Solenoid', 'RF', 'Location', 'north', 'Orientation', 'horizontal', 'Box', 'off')
        
        linkaxes([ax3 ax4], 'x')

end