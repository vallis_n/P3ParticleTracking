clear;

% % WP1 50
% RUNpath = 'astra/SWP_1902_094123';
% RUNid =    {'RUN_1902_094123', 'RUN_1902_094235', ...
%             'RUN_1902_094350', 'RUN_1902_094508', ...
%             'RUN_1902_094628', 'RUN_1902_094749'};
% outname =  {'g4_FCs/wp1_212_mT.dat', 'g4_FCs/wp1_120_mT.dat', ...
%             'g4_FCs/wp1_068_mT.dat', 'g4_FCs/wp1_038_mT.dat', ...
%             'g4_FCs/wp1_021_mT.dat', 'g4_FCs/wp1_012_mT.dat'};

% % WP2 50
% RUNpath = 'astra/SWP_1602_165736';
% RUNid =    {'RUN_1602_165736', 'RUN_1602_165842', ...
%             'RUN_1602_165954', 'RUN_1602_170109', ...
%             'RUN_1602_170228', 'RUN_1602_170348'};
% outname =  {'g4_FCs/wp2_212_mT.dat', 'g4_FCs/wp2_120_mT.dat', ...
%             'g4_FCs/wp2_068_mT.dat', 'g4_FCs/wp2_038_mT.dat', ...
%             'g4_FCs/wp2_021_mT.dat', 'g4_FCs/wp2_012_mT.dat'};

% % WP1 12.5
% RUNpath = 'astra/SWP_1902_104559';
% RUNid =   'RUN_1902_094910';
% outname = 'g4_FCs/wp1_053_mT.dat';

% WP2 12.5
RUNpath = 'astra/SWP_1902_104559';
RUNid =   'RUN_1902_104717';
outname = 'g4_FCs/wp2_053_mT.dat';

% for bbb = 1:6
%     astra2G4_FC(RUNpath, RUNid{bbb}, outname{bbb})
% end

astra2G4_FC(RUNpath, RUNid, outname)