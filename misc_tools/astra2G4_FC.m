function L = astra2G4_FC(RUNpath, RUNid, outname)
    filename = [RUNpath '/' RUNid '/' RUNid '.0550.001'];
    load([RUNpath '/' RUNid '/dat/' RUNid '_ASTRA_parameters.mat']);
    
    %fcup_p = ASTRA.fcup_p;
    %fcup_p = [3600+260/2*cosd(85)*[-1 1]; -(165+260/2*sind(85)*[1 -1])]; %fc12
    % Fcup = [3550+80/2*cosd(75)*[-1 1]; -(100+sind(75)*80) -100]; %fc50
    % fcup_p = Fcup;
    %fcup_p = [339.6 360.4; -178.6 -101.3]/1e3;
    fcup_p = [389 411; -294.5 -35.5]/1e3; %fc12
    
    
    % Get distribution
    opts = detectImportOptions(filename, 'FileType', 'text');
    DIST = readtable(filename,opts);
    DIST = table2array(DIST);
    REF = DIST(1,:);
    DIST = DIST(DIST(:,10) == 5 & DIST(:,9)==2,:);
    DIST = DIST(DIST(:,10) == 5 & DIST(:,3)+REF(3) > ASTRA.D_pos+ASTRA.D_drift,:);
    
    % Reformat: [x_corrected y_corrected x' y' species]
    z = DIST(:,3)+REF(3)-3.2;
    x = DIST(:,1);
    y = DIST(:,2);
    x_prime = DIST(:,4)./(DIST(:,6)+REF(6));
    y_prime = DIST(:,5)./(DIST(:,6)+REF(6));
    x_0 = x - z.*x_prime;
    y_0 = y - z.*y_prime;
    f_prime = (fcup_p(2,2)-fcup_p(2,1))/(fcup_p(1,2)-fcup_p(1,1));
    f_0 = fcup_p(2,1) - fcup_p(1,1)*f_prime;
    z_dist_fcup = (f_0-x_0)./(x_prime-f_prime);
    % z_dist_fcup = ASTRA.fcup_p(1,2)*ones(length(x),1);
    x_dist_fcup  = x_0+x_prime.*z_dist_fcup;
    y_dist_fcup = y_0+y_prime.*z_dist_fcup;
    E = sqrt(0.511^2 + (DIST(:,6)+DIST(1,6)).^2 + DIST(:,4).^2 + DIST(:,5).^2);
    
    % Translation and Rotation
    %alpha = 90-rad2deg(f_prime^-1);
    x_center = sum(fcup_p(2,:))/2;
    z_center = sum(fcup_p(1,:))/2;
    x_t = x_dist_fcup -  x_center;
    z_t = z_dist_fcup -  z_center;
    alpha = -1*(90-atand(f_prime));
    x_rot = cosd(alpha)*x_t - sind(alpha)*z_t;
    z_rot = sind(alpha)*x_t + cosd(alpha)*z_t;
    x_prime_rot = tand(atand(x_prime) - alpha);
    
    % g4DIST = [x_rot*1e3-115 atan(x_prime+f_prime^-1)*1e3 y_dist_fcup*1e3 atan(y_prime)*1e3 z_rot*1e3 E/1e6];
    g4DIST = [x_rot*1e3 atan(x_prime_rot)*1e3 y_dist_fcup*1e3 atan(y_prime)*1e3 z_rot*1e3 E/1e6];
    L = length(g4DIST);
    
    writematrix(g4DIST, outname, 'Delimiter', 'tab')
    
    vq = 0:0.001:2;
    figure(1)
    plot(vq, [f_prime.*vq]+f_0)
    hold on
    scatter(fcup_p(1,:), fcup_p(2,:))
    for iii=1:50
        plot(vq,x_prime(iii).*vq+x_0(iii), 'k')
    end
    scatter(z_dist_fcup(1:50), x_dist_fcup(1:50))
    hold off
    
    figure(2)
    scatter(z_dist_fcup*1e3, x_dist_fcup*1e3, 1)
    hold on
    quiver(z_dist_fcup*1e3, x_dist_fcup*1e3, cos(atan(x_prime)), sin(atan(x_prime)))
    scatter(z_rot*1e3, x_rot*1e3, 1, E)
    quiver(z_rot*1e3, x_rot*1e3, cos(atan(x_prime_rot)), sin(atan(x_prime_rot)))
    scatter(fcup_p(1,:)*1e3, fcup_p(2,:)*1e3, 'filled')
        clim([1 100]*1e6)
    hold off
    pbaspect([1 1 1])

end
