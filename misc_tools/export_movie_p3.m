function export_movie_p3(RUN_id, RUN_path, RUN_index, ASTRA, FOM)
        
    movie_out_long = ['movie_tmp/' RUN_id '_long.gif'];
    movie_out_tran = ['movie_tmp/' RUN_id '_tran.gif'];
    L = length(RUN_index.z);
    w = waitbar(0,'writing movie...');
    
    for zzz = 1:L
        waitbar(zzz/L);
        fff = RUN_index.files{zzz};
        zzz = RUN_index.z(zzz);
        % Longitudinal
        fig_long = plot_long_p3([RUN_path '/' RUN_id '/' fff], RUN_index, ASTRA, FOM, false);
        fffig_long = getframe(fig_long);
        fffig_long = frame2im(fffig_long);
        [imind_long,cm_long] = rgb2ind(fffig_long,256);
        if zzz == 0
            imwrite(imind_long,cm_long,movie_out_long,'gif', 'Loopcount',inf,'DelayTime',1);
        else
            imwrite(imind_long,cm_long,movie_out_long,'gif','WriteMode','append','DelayTime',0.1); 
        end
    end
        close(w)
    end