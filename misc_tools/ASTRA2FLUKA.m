clear; close all;
opts = detectImportOptions('RUN_1710_175445.0280.001','FileType','text');
DIST = readtable('RUN_2106_145808.0280.001',opts);
DIST = table2array(DIST);
REF = DIST(1,:);
DIST = DIST(DIST(:,10)==5,:);
DIST(1,:) = [];

flukaDIST = zeros(length(DIST),10);
% partID
flukaDIST(:,1) = 1:length(DIST)';
flukaDIST(:,2) = DIST(:,9) + 2;
flukaDIST(:,3) = (sqrt(DIST(:,4).^2 + DIST(:,5).^2 + (REF(6)+DIST(:,6)).^2))/1e9;
flukaDIST(:,4) = DIST(:,1); flukaDIST(:,5) = DIST(:,2); flukaDIST(:,6) = DIST(:,3)+REF(3);
flukaDIST(:,7) = (DIST(:,4)/1e9)./flukaDIST(:,3);
flukaDIST(:,8) = (DIST(:,5)/1e9)./flukaDIST(:,3);
flukaDIST(:,9) = ((DIST(:,6)+REF(6))/1e9)./flukaDIST(:,3);
flukaDIST(:,10) = 1;

writematrix(flukaDIST, 'flukaDIST.txt')