clear; close all;
sol = readmatrix('NCSolenoid_1_piece.txt');
z1 = 0.202; z2 = 0.164;
z = -3:0.001:3; z = z';
sol(1,1) = -3; sol(1,2) = 0; 
sol(end,1) = 3; sol(end,2) = 0; 

%% BUILD SOLENOID FIELD
SOL = zeros(length(z),1);
for sss = 0:1
    AddField = interp1(sol(:,1)-z2/2-sss*z2,sol(:,2),z);
    AddField(isnan(AddField)) = 0;
    SOL = SOL + AddField;
    AddField = interp1(sol(:,1)+z2/2+sss*z2,sol(:,2),z);
    AddField(isnan(AddField)) = 0;
    SOL = SOL + AddField;
end

zASTRA = 0:0.001:15; zASTRA = zASTRA';
ASTRAField = zeros(length(zASTRA),1);
CPos = 0.85 + 1.35*[0:9];
DzSol = (z1+3*z2)/2;
for ccc = 1:10
    AddField = interp1(z+DzSol+CPos(ccc), SOL, zASTRA);
    AddField(isnan(AddField)) = 0;
    ASTRAField = ASTRAField + AddField;
    AddField = interp1(z-DzSol+CPos(ccc), SOL, zASTRA);
    AddField(isnan(AddField)) = 0;
    ASTRAField = ASTRAField + AddField;
end

%% PLOT
figure()
plot(z,SOL)
figure()
plot(zASTRA,ASTRAField)

writematrix([z SOL],'NCSolenoid_4_pieces.txt')